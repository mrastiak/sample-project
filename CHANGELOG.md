# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/mrastiak/sample-project/compare/v0.1.1...v0.1.2) (2021-06-10)

### Features

- add error handling to useFetch hook ([c3b9e52](https://gitlab.com/mrastiak/sample-project/commit/c3b9e5272a06f92428c1996b3bdab4565d14368f))

### [0.1.1](https://gitlab.com/mrastiak/sample-project/compare/v0.1.0...v0.1.1) (2021-06-10)

### Features

- add prop types to components ([210f5b1](https://gitlab.com/mrastiak/sample-project/commit/210f5b1883ce93d609d073cdd71149e6e6bc35ab))
- add useClickout hook ([e124a3c](https://gitlab.com/mrastiak/sample-project/commit/e124a3c92e79c9d5ed11dbdcee1be35f93d60c60))

### Bug Fixes

- remove unnecessary packages ([0ffb09a](https://gitlab.com/mrastiak/sample-project/commit/0ffb09a192148a5eed01bcee3ea6c6e51ab42b9f))

## 0.1.0 (2021-06-10)

### Features

- add api call to cars page ([8f61437](https://gitlab.com/mrastiak/sample-project/commit/8f6143735b3fca0689345cbfc67089504d319924))
- add button component ([b7d5ba3](https://gitlab.com/mrastiak/sample-project/commit/b7d5ba3cc8218eb3c8f6475a15eaac88654352f5))
- add commitlint and husky ([b6c6209](https://gitlab.com/mrastiak/sample-project/commit/b6c6209917efeb466b5c4f479cd6b30c9a6d452c))
- add favourite functionality ([c83521c](https://gitlab.com/mrastiak/sample-project/commit/c83521c0ebab875f56ba1e2337ba3e0bd092dad5))
- add header and footer ([db01c1b](https://gitlab.com/mrastiak/sample-project/commit/db01c1b6829bc2e28afc55ef7f580dac6524a49c))
- add initial files using create react app ([ee52585](https://gitlab.com/mrastiak/sample-project/commit/ee5258591b2214a7f7e76e18c127bb11e14caeb3))
- add jsconfig for absolute import ([2c8dcc8](https://gitlab.com/mrastiak/sample-project/commit/2c8dcc890ffc4b34c056721768f21110504a0fd7))
- add lint staged ([4cf96ca](https://gitlab.com/mrastiak/sample-project/commit/4cf96ca6837fccaac9f714f24ead7a33acf9de29))
- add not found page ([a6c169b](https://gitlab.com/mrastiak/sample-project/commit/a6c169b7507be0a98827686fe58cecea87ad1051))
- add router ([c4a4867](https://gitlab.com/mrastiak/sample-project/commit/c4a486705ac21e594b03ed59baf479a61019f6a4))
- add sass ([468daeb](https://gitlab.com/mrastiak/sample-project/commit/468daebd89df140f8ef1c5cb3d0b5d254bd069c4))
- add select component ([0d10458](https://gitlab.com/mrastiak/sample-project/commit/0d10458bc2e340d85f4dde8937a97d1d6f9d0e63))
- add sort to cars ([f27f2e8](https://gitlab.com/mrastiak/sample-project/commit/f27f2e8d66bd6d7c61406516832d62bffd2dd216))
- add standard-version ([6b24059](https://gitlab.com/mrastiak/sample-project/commit/6b24059edfe8c02c791af6382ee4a526ee3d07d2))
- add useFetch hook and api call to car detail page ([e031ff8](https://gitlab.com/mrastiak/sample-project/commit/e031ff84f43d517a4dac723a5d74d3526878fee7))
