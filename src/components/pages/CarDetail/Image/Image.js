import PropTypes from "prop-types";
import style from "./Image.module.scss";

const Image = ({ src }) => {
  return (
    <div className={style.image}>
      <img src={src} alt="Car" />
    </div>
  );
};

Image.propTypes = {
  src: PropTypes.string.isRequired,
};

export default Image;
