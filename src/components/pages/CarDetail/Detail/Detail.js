import PropTypes from "prop-types";
import Button from "components/common/Button";
import style from "./Detail.module.scss";
import capitalizeFirstLetter from "utils/capitalizeFirstLetter";

const Detail = ({ data, isFavourite, addToFavourite, removeFromFavourite }) => {
  return (
    <div className={style.container}>
      <div className={style.detail}>
        <h1 className={style.title}>
          {data.manufacturerName} {data.modelName}
        </h1>
        <p className={style.subtitle}>
          Stock #{data.stockNumber} - {data.mileage.number}{" "}
          {data.mileage.unit.toUpperCase()} - {data.fuelType} -{" "}
          {capitalizeFirstLetter(data.color)}
        </p>
        <p className={style.description}>
          This car is currently available and can be delivered as soon as
          tomorrow morning. Please be aware that delivery times shown in this
          page are not definitive and may change due to bad weather conditions.
        </p>
      </div>
      <div className={style.favourite}>
        <p>
          {isFavourite
            ? `This car is already in your favourites, click the button to remove it from your collection
            of favourite items.`
            : `If you like this car, click the button and save it in your collection
          of favourite items.`}
        </p>
        {isFavourite ? (
          <Button onClick={removeFromFavourite}>Remove</Button>
        ) : (
          <Button onClick={addToFavourite}>Save</Button>
        )}
      </div>
    </div>
  );
};

Detail.propTypes = {
  data: PropTypes.object.isRequired,
  isFavourite: PropTypes.bool.isRequired,
  addToFavourite: PropTypes.func.isRequired,
  removeFromFavourite: PropTypes.func.isRequired,
};

export default Detail;
