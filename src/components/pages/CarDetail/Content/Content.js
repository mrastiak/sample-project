import PropTypes from "prop-types";
import Image from "components/pages/CarDetail/Image";
import Detail from "components/pages/CarDetail/Detail";
import style from "./Content.module.scss";

const Content = ({
  data,
  loading,
  isFavourite,
  addToFavourite,
  removeFromFavourite,
}) => {
  if (loading) {
    return <div>Loading</div>;
  }

  return (
    <div className={style.content}>
      <Image src={data.pictureUrl} />
      <Detail
        addToFavourite={addToFavourite}
        removeFromFavourite={removeFromFavourite}
        data={data}
        isFavourite={isFavourite}
      />
    </div>
  );
};

Content.propTypes = {
  data: PropTypes.object,
  loading: PropTypes.bool.isRequired,
  isFavourite: PropTypes.bool.isRequired,
  addToFavourite: PropTypes.func.isRequired,
  removeFromFavourite: PropTypes.func.isRequired,
};

Content.defaultProps = {
  data: {},
};

export default Content;
