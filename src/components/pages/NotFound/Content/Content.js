import style from "./Content.module.scss";
import Link from "components/common/Link";
import paths from "router/paths";

const Content = () => {
  return (
    <div className={style.content}>
      <img
        src={`${process.env.REACT_APP_ASSETS_BASE_URL}/images/logo.png`}
        alt="AUTO1"
        className={style.logo}
      />
      <h1 className={style.title}>404 not found</h1>
      <p>Sorry, the page you are looking for does not exist.</p>
      <p>
        You can always go back to the <Link to={paths.cars}>homepage</Link>.
      </p>
    </div>
  );
};

export default Content;
