import PropTypes from "prop-types";
import Card from "components/pages/Cars/Card";
import Loading from "components/pages/Cars/Loading";
import style from "./List.module.scss";

const List = ({ cars, loading, totalCarsCount }) => {
  if (loading) {
    return (
      <div className={style.cards}>
        {Array(10)
          .fill()
          .map((_, index) => (
            <Loading key={index} />
          ))}
      </div>
    );
  }

  return (
    <div className={style.list}>
      <h1 className={style.title}>Available Cars</h1>
      <p className={style.subtitle}>Showing 10 of {totalCarsCount} results</p>
      <div className={style.cards}>
        {cars.map((car) => (
          <Card key={car.stockNumber} car={car} />
        ))}
      </div>
    </div>
  );
};

List.propTypes = {
  cars: PropTypes.arrayOf(PropTypes.object).isRequired,
  loading: PropTypes.bool.isRequired,
  totalCarsCount: PropTypes.number.isRequired,
};

export default List;
