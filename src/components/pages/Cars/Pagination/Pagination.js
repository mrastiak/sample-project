import PropTypes from "prop-types";
import cx from "classnames";
import Link from "components/common/Link";
import style from "./Pagination.module.scss";

const Pagination = ({ className, page, totalPageCount, onPageChange }) => {
  return (
    <div className={cx(style.pagination, className)}>
      {page > 1 && (
        <>
          <Link className={style.actions} onClick={() => onPageChange(1)}>
            First
          </Link>
          <Link
            className={style.actions}
            onClick={() => onPageChange(page - 1)}
          >
            Previous
          </Link>
        </>
      )}
      <p>
        Page {page} of {totalPageCount}
      </p>
      {page < totalPageCount && (
        <>
          <Link
            className={style.actions}
            onClick={() => onPageChange(page + 1)}
          >
            Next
          </Link>
          <Link
            className={style.actions}
            onClick={() => onPageChange(totalPageCount)}
          >
            Last
          </Link>
        </>
      )}
    </div>
  );
};

Pagination.propTypes = {
  className: PropTypes.string,
  page: PropTypes.number.isRequired,
  totalPageCount: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

Pagination.defaultProps = {
  className: "",
};

export default Pagination;
