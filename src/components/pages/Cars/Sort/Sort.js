import { useRef, useState } from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import Select from "components/common/Select";
import Button from "components/common/Button";
import style from "./Sort.module.scss";

const Sort = ({ className, onSubmit, loading }) => {
  const sortOptions = useRef([
    {
      title: "None",
      value: 0,
    },
    {
      title: "Mileage - Ascending",
      value: "asc",
    },
    {
      title: "Mileage - Descending",
      value: "des",
    },
  ]);

  const [selectedSort, setSelectedSort] = useState(
    sortOptions.current[0].value
  );

  const handleSubmit = () => {
    onSubmit(selectedSort);
  };

  return (
    <div className={cx(style.sort, className)}>
      <Select
        value={selectedSort}
        onChange={setSelectedSort}
        options={sortOptions.current}
        disabled={loading}
      >
        Sort by
      </Select>

      <Button
        className={style.action}
        onClick={handleSubmit}
        disabled={loading}
      >
        Update
      </Button>
    </div>
  );
};

Sort.propTypes = {
  className: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

Sort.defaultProps = {
  className: "",
};

export default Sort;
