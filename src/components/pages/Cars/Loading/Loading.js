import style from "./Loading.module.scss";

const Loading = () => {
  return (
    <div className={style.card}>
      <div className={style.image}></div>
      <div className={style.content}>
        <div className={style.title}></div>
        <div className={style.subtitle}></div>
        <div className={style.action}></div>
      </div>
    </div>
  );
};

export default Loading;
