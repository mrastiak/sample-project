import PropTypes from "prop-types";
import Filter from "components/pages/Cars/Filter";
import Sort from "components/pages/Cars/Sort";
import List from "components/pages/Cars/List";
import Pagination from "components/pages/Cars/Pagination";
import style from "./Content.module.scss";

const Content = ({
  cars,
  loading,
  page,
  onPageChange,
  onSortChange,
  onFiltersChnage,
  totalCarsCount,
  totalPageCount,
}) => {
  return (
    <div className={style.container}>
      <div className={style.actions}>
        <Filter
          className={style.filter}
          onSubmit={onFiltersChnage}
          loading={loading}
        />
        <Sort
          className={style.sort}
          onSubmit={onSortChange}
          loading={loading}
        />
      </div>
      <div className={style.content}>
        <List loading={loading} cars={cars} totalCarsCount={totalCarsCount} />
        {!loading && (
          <Pagination
            className={style.pagination}
            page={page}
            totalPageCount={totalPageCount}
            onPageChange={onPageChange}
          />
        )}
      </div>
    </div>
  );
};

Content.propTypes = {
  cars: PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  onSortChange: PropTypes.func.isRequired,
  onFiltersChnage: PropTypes.func.isRequired,
  totalCarsCount: PropTypes.number,
  totalPageCount: PropTypes.number,
};

Content.defaultProps = {
  cars: [],
  totalCarsCount: 0,
  totalPageCount: 0,
};

export default Content;
