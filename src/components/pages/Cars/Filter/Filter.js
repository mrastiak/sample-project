import { useMemo, useRef, useState } from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import Select from "components/common/Select";
import Button from "components/common/Button";
import style from "./Filter.module.scss";
import useFetch from "hooks/useFetch";
import capitalizeFirstLetter from "utils/capitalizeFirstLetter";

const Filter = ({ className, onSubmit, loading }) => {
  const manufacturerPlaceholder = useRef({
    title: "All manufacturers",
    value: 0,
  });
  const colorPlaceholder = useRef({
    title: "All car colors",
    value: 0,
  });
  const { response: manufacturers, loading: manufacturersLoading } =
    useFetch("manufacturers");
  const manufacturerOptions = useMemo(() => {
    return (
      manufacturers.manufacturers?.reduce(
        (acc, currentValue) => {
          acc.push({
            title: currentValue.name,
            value: currentValue.name,
          });
          return acc;
        },
        [manufacturerPlaceholder.current]
      ) || [manufacturerPlaceholder.current]
    );
  }, [manufacturers]);
  const [selectedManufacturer, setSelectedManufacturer] = useState(
    manufacturerOptions[0].value
  );

  const { response: colors, loading: colorsLoading } = useFetch("colors");
  const colorOptions = useMemo(() => {
    return (
      colors.colors?.reduce(
        (acc, currentValue) => {
          acc.push({
            title: capitalizeFirstLetter(currentValue),
            value: currentValue,
          });
          return acc;
        },
        [colorPlaceholder.current]
      ) || [colorPlaceholder.current]
    );
  }, [colors]);

  const [selectedColor, setSelectedColor] = useState(colorOptions[0].value);

  const handleSubmit = () => {
    onSubmit({
      manufacturer: selectedManufacturer,
      color: selectedColor,
    });
  };

  return (
    <div className={cx(style.filter, className)}>
      <Select
        value={selectedColor}
        onChange={setSelectedColor}
        options={colorOptions}
        disabled={loading || colorsLoading}
      >
        Color
      </Select>

      <Select
        value={selectedManufacturer}
        onChange={setSelectedManufacturer}
        options={manufacturerOptions}
        disabled={loading || manufacturersLoading}
      >
        Manufacturer
      </Select>
      <Button
        className={style.action}
        onClick={handleSubmit}
        disabled={loading || manufacturersLoading || colorsLoading}
      >
        Filter
      </Button>
    </div>
  );
};

Filter.propTypes = {
  className: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

Filter.defaultProps = {
  className: "",
};

export default Filter;
