import PropTypes from "prop-types";
import Link from "components/common/Link";
import paths from "router/paths";
import capitalizeFirstLetter from "utils/capitalizeFirstLetter";
import style from "./Card.module.scss";

const Card = ({ car }) => {
  return (
    <div className={style.card}>
      <div className={style.image}>
        <img src={car.pictureUrl} alt={`Car #${car.stockNumber}`} />
      </div>
      <div className={style.content}>
        <p className={style.title}>
          {car.manufacturerName} {car.modelName}
        </p>
        <p className={style.subtitle}>
          Stock #{car.stockNumber} - {car.mileage.number}{" "}
          {car.mileage.unit.toUpperCase()} - {car.fuelType} -{" "}
          {capitalizeFirstLetter(car.color)}
        </p>
        <Link className={style.action} to={paths.carDetail(car.stockNumber)}>
          View details
        </Link>
      </div>
    </div>
  );
};

Card.propTypes = {
  car: PropTypes.object.isRequired,
};

export default Card;
