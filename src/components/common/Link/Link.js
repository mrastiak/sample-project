import { Link as RouterLink } from "react-router-dom";
import cx from "classnames";
import PropTypes from "prop-types";
import style from "./Link.module.scss";

const Link = ({ children, className, to, onClick, ...rest }) => {
  if (to) {
    return (
      <RouterLink className={cx(style.link, className)} to={to} {...rest}>
        {children}
      </RouterLink>
    );
  }

  if (onClick) {
    return (
      <span className={cx(style.link, className)} onClick={onClick} {...rest}>
        {children}
      </span>
    );
  }

  return null;
};

Link.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
  to: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  onClick: PropTypes.func,
};

Link.defaultProps = {
  className: "",
  to: null,
  onClick: null,
};

export default Link;
