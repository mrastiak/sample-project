import { useHistory } from "react-router-dom";
import paths from "router/paths";
import style from "./Header.module.scss";

const Header = () => {
  const history = useHistory();

  return (
    <header className={style.header}>
      <img
        src={`${process.env.REACT_APP_ASSETS_BASE_URL}/images/logo.png`}
        alt="AUTO1"
        className={style.logo}
        onClick={() => history.push(paths.cars)}
      />
      <nav>
        <ul className={style.nav}>
          <li>Purchase</li>
          <li>My Orders</li>
          <li>Sell</li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
