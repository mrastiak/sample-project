import cx from "classnames";
import PropTypes from "prop-types";
import style from "./Button.module.scss";

const Button = ({ className, children, ...rest }) => {
  return (
    <button className={cx(style.button, className)} {...rest}>
      {children}
    </button>
  );
};

Button.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
};

Button.defaultProps = {
  className: "",
};

export default Button;
