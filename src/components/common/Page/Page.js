import PropTypes from "prop-types";
import Header from "components/common/Header";
import Footer from "components/common/Footer";
import style from "./Page.module.scss";

const Page = ({ children }) => {
  return (
    <div className={style.page}>
      <Header />
      <main className={style.main}>{children}</main>
      <Footer />
    </div>
  );
};

Page.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.element]).isRequired,
};

export default Page;
