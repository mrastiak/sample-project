import { useCallback, useMemo, useRef, useState } from "react";
import cx from "classnames";
import PropTypes from "prop-types";
import useClickout from "hooks/useClickout";
import style from "./Select.module.scss";

const Select = ({
  options,
  className,
  value,
  onChange,
  children,
  disabled,
  ...rest
}) => {
  const ref = useRef();
  const [isOpen, setIsOpen] = useState(false);

  const selectedTitle = useMemo(() => {
    return options.find((option) => option.value === value)?.title;
  }, [options, value]);

  const handleToggle = () => {
    if (disabled) {
      return;
    }
    setIsOpen(!isOpen);
  };

  const handleClose = useCallback(() => {
    setIsOpen(false);
  }, []);

  useClickout(ref, handleClose);

  return (
    <div
      className={cx(style.container, className)}
      onClick={handleToggle}
      {...rest}
      ref={ref}
    >
      {children && <p className={style.label}>{children}</p>}
      <div className={cx(style.select, { [style.open]: isOpen })}>
        {selectedTitle}
        {isOpen && (
          <div className={style.dropdown}>
            {options.map((option) => (
              <div
                className={cx(style.option, {
                  [style.selected]: value === option.value,
                })}
                key={option.title}
                onClick={() => onChange(option.value)}
              >
                {option.title}
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

Select.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.exact({
      title: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    })
  ).isRequired,
  className: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  onChange: PropTypes.func.isRequired,
  children: PropTypes.node,
  disabled: PropTypes.bool,
};

Select.defaultProps = {
  className: "",
  children: null,
  disabled: false,
};

export default Select;
