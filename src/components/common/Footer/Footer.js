import style from "./Footer.module.scss";

const Footer = () => {
  return <footer className={style.footer}>&copy; AUTO1 Group 2018</footer>;
};

export default Footer;
