import { useCallback, useEffect } from "react";

const useClickout = (ref, action) => {
  const handleClick = useCallback(
    (e) => {
      if (!ref.current || ref.current.contains(e.target)) {
        return;
      }
      action();
    },
    [ref, action]
  );
  useEffect(() => {
    document.addEventListener("click", handleClick);

    return () => {
      document.removeEventListener("click", handleClick);
    };
  }, [handleClick]);
};

export default useClickout;
