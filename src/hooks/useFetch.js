import { useCallback, useEffect, useMemo, useRef, useState } from "react";

const useFetch = (url, userOptions, initialLoad = true) => {
  const isMounted = useRef(false);
  const [loading, setLoading] = useState(initialLoad);
  const [error, setError] = useState();
  const [response, setResponse] = useState({});
  const defaultHeaders = useRef({
    "Content-Type": "application/json",
  });

  const options = useMemo(() => {
    const headers = { ...defaultHeaders, ...userOptions?.headers };
    return { ...userOptions, headers };
  }, [userOptions]);

  const handleResponse = (res) => {
    if (!res.ok) {
      const error = new Error(res.statusText);
      Object.assign(error, {
        status: res.status,
        statusText: res.statusText,
        body: res.body,
      });
      throw error;
    }
    return res.json();
  };

  const apiCall = useCallback(async () => {
    try {
      setError(null);
      setLoading(true);
      const res = await fetch(
        `${process.env.REACT_APP_API_BASE_URL}/api/${url}`,
        options
      );

      const json = await handleResponse(res);
      setResponse(json);
      setLoading(false);
    } catch (e) {
      setError(e);
    }
  }, [url, options]);

  useEffect(() => {
    if (initialLoad || isMounted.current) {
      apiCall();
    }
    isMounted.current = true;
  }, [initialLoad, apiCall]);

  return {
    response,
    loading,
    error,
  };
};

export default useFetch;
