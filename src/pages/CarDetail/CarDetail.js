import { useEffect, useMemo, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";
import Page from "components/common/Page";
import Content from "components/pages/CarDetail/Content";
import useFetch from "hooks/useFetch";
import { getItem, setItem } from "utils/storage";
import paths from "router/paths";

const CarDetail = ({ match }) => {
  const history = useHistory();
  const [favouriteCars, setFavouriteCars] = useState(
    getItem("favouritesCars") || []
  );

  const isFavourite = useMemo(() => {
    return favouriteCars.includes(match.params.carId);
  }, [favouriteCars, match]);

  const url = useMemo(() => {
    return `cars/${match.params.carId}`;
  }, [match]);

  const addToFavourite = () => {
    const newFavourites = new Set(favouriteCars).add(match.params.carId);
    setFavouriteCars([...newFavourites]);
  };
  const removeFromFavourite = () => {
    const favouritesSet = new Set(favouriteCars);
    favouritesSet.delete(match.params.carId);
    setFavouriteCars([...favouritesSet]);
  };

  useEffect(() => {
    setItem("favouritesCars", favouriteCars);
  }, [favouriteCars]);

  const { response, loading, error } = useFetch(url);

  useEffect(() => {
    if (error && error.status === 404) {
      history.push(paths.notFound);
    }
  }, [error, history]);

  return (
    <Page>
      <Content
        data={response.car}
        loading={loading}
        isFavourite={isFavourite}
        addToFavourite={addToFavourite}
        removeFromFavourite={removeFromFavourite}
      />
    </Page>
  );
};

CarDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      carId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default CarDetail;
