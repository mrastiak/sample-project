import { useMemo, useState } from "react";
import Page from "components/common/Page";
import Content from "components/pages/Cars/Content";
import useFetch from "hooks/useFetch";

const Cars = () => {
  const [filters, setFilters] = useState({
    manufacturer: undefined,
    color: undefined,
  });
  const [page, setPage] = useState(1);
  const [sort, setSort] = useState();

  const url = useMemo(() => {
    const queryObject = { page, sort, ...filters };
    const queryString = Object.keys(queryObject)
      .filter((key) => queryObject[key])
      .map((key) => `${key}=${queryObject[key]}`)
      .join("&");
    return `cars?${queryString}`;
  }, [page, sort, filters]);

  const { response, loading } = useFetch(url);

  const totalCarsCount = useMemo(() => {
    return response.totalCarsCount;
  }, [response.totalCarsCount]);

  const totalPageCount = useMemo(() => {
    return response.totalPageCount;
  }, [response.totalPageCount]);

  const resetPagination = () => {
    setPage(1);
  };

  return (
    <Page>
      <Content
        loading={loading}
        cars={response.cars}
        sort={sort}
        onSortChange={(value) => {
          resetPagination();
          setSort(value);
        }}
        page={page}
        onPageChange={setPage}
        onFiltersChnage={(value) => {
          resetPagination();
          setFilters(value);
        }}
        totalCarsCount={totalCarsCount}
        totalPageCount={totalPageCount}
      />
    </Page>
  );
};

export default Cars;
