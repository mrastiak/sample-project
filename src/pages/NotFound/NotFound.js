import Page from "components/common/Page";
import Content from "components/pages/NotFound/Content";

const NotFound = () => {
  return (
    <Page>
      <Content />
    </Page>
  );
};

export default NotFound;
