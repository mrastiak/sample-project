const paths = {
  cars: "/",
  notFound: "/not-found",
  carDetail: (id) => `/${id || ":carId"}`,
};

export default paths;
