import paths from "router/paths";
import Cars from "pages/Cars";
import CarDetail from "pages/CarDetail";
import NotFound from "pages/NotFound";

const routes = [
  {
    component: Cars,
    path: paths.cars,
  },
  {
    component: NotFound,
    path: paths.notFound,
  },
  {
    component: CarDetail,
    path: paths.carDetail(),
  },
];

export default routes;
