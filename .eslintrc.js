module.exports = {
  extends: ["react-app", "react-app/jest", "prettier"],
  rules: {
    "react/prop-types": ["error"],
    "react/require-default-props": ["error"],
  },
};
